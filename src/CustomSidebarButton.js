import React from 'react';
import {SideLink, Actions} from "@twilio/flex-ui";

const CustomSidebarButton = ({activeView}) => {
    function navigate() {
        Actions.invokeAction('NavigateToView', {viewName: 'custom-view'});
    }
    return(
        <SideLink
            showLabel={true}
            icon={
                <svg width="24" height="24"viewBox="0 0 24 24">
                    <circle cx="12" cy="8" r="8" fill="#eaeaea" />
                    <line x1="12" y1="8" x2="0" y2="24" stroke="#FFFFFF" />
                    <circle cx="12" cy="7" r="6" fill="#ffffff" />
                </svg>
            }
            iconActive={
                <svg width="24" height="24"viewBox="0 0 24 24">
                    <circle cx="12" cy="8" r="8" fill="#eaeaea" />
                    <line x1="12" y1="8" x2="0" y2="24" stroke="#FFFFFF" />
                    <circle cx="12" cy="7" r="6" fill="#FFFFFF" />
                </svg>
            }
            isActive={activeView === 'custom-view'}
            onClick={navigate}
        >
            Search
        </SideLink>

    );
}

export default CustomSidebarButton