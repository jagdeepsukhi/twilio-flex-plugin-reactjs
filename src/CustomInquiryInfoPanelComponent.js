import React from 'react';


const CustomInquiryInfoPanelComponent = (props) => {

	let taskData = props.task;

	console.log('dataval', props);
	 
	let taskID = taskData.taskSid ; 
	//props.conference.source.sid
	var dateV = new Date(taskData.dateCreated).toString(); 
 	//var elapsed = date.getTime();


    return <div class="Twilio-TaskInfoPanel-default css-18ljn0d"><span class="Twilio">

    <h1>TASK CONTEXT</h1>
    <h2>Task Channel Type</h2>
	<p>{taskData.channelType || "unknown"}</p>
	<h2>Task created on</h2>
	<p>{ dateV }  </p>
	<h2>Task priority</h2>
	<p>{taskData.priority || "0"}</p>
	<h2>Task queue</h2>
	<p>{taskData.workflowName}</p>
	<h2>Task ID</h2>
	<p> {taskID || ""}</p>
	<h2>Reservation Sid</h2>
	<p>{taskData._reservation.sid || ""}</p>
	<hr></hr>
	<h1>MANAGER CONTEXT</h1>
	<h2>Priority</h2>
	<p>{taskData.attributes.priority || ""} </p>
	<h2>Task Type</h2>
	<p>{taskData.attributes.task_type || ""} </p>
	<h2>Message</h2>
	<p>{taskData.attributes.description || ""}</p>
    </span></div>;

};

export default CustomInquiryInfoPanelComponent;





