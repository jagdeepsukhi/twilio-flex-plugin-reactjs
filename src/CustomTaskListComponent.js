import React from 'react';

const taskListStyles = {
  padding: '10px',
  margin: '0px',
  color: '#fff',
  background: 'green',
};

const CustomTaskListComponent = () => {
  return <div style={taskListStyles}>Hello! New Messages will appear below.</div>;
};

export default CustomTaskListComponent;
