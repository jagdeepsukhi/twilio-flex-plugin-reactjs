import { FlexPlugin } from 'flex-plugin';
import {View} from "@twilio/flex-ui";
import React from 'react';
import CustomSidebarButton from './CustomSidebarButton';
import TaskSidebarButton from "./TaskSidebarButton";
import HelpSidebarButton from "./HelpSidebarButton";
import EmailSidebarButton from "./EmailSidebarButton";
import CalendarSidebarButton from "./CalendarSidebarButton";
import OffersheetSidebarButton from "./OffersheetSidebarButton";
import OffersheetsSidebarButton from "./OffersheetsSidebarButton";
import CustomView from './CustomView';
import TaskView from "./TaskView";
import HelpView from "./HelpView";
import EmailView from "./EmailView";
import CalendarView from "./CalendarView";
import OffersheetView from "./OffersheetView";
import OffersheetsView from "./OffersheetsView";

const PLUGIN_NAME = 'AddCustomViewPlugin';




export default class AddCustomViewPlugin extends FlexPlugin {
    constructor() {
        super(PLUGIN_NAME);
    }

    /**
     * This code is run when your plugin is being started
     * Use this to modify any UI components or attach to the actions framework
     *
     * @param flex { typeof import('@twilio/flex-ui') }
     * @param manager { import('@twilio/flex-ui').Manager }
     */
    init(flex, manager) {
        flex.SideNav.Content.add(
            <CustomSidebarButton key="custon-view-button"/>
        );

        flex.SideNav.Content.add(
            <TaskSidebarButton key="task-view-button"/>
        );

        flex.SideNav.Content.add(
            <EmailSidebarButton key="email-view-button"/>
        );

        flex.SideNav.Content.add(
            <OffersheetSidebarButton key="offersheet-view-button"/>
        );

        flex.SideNav.Content.add(
            <OffersheetsSidebarButton key="offersheets-view-button"/>
        );

        flex.SideNav.Content.add(
            <CalendarSidebarButton key="caldendar-view-button"/>
        );

        flex.SideNav.Content.add(
            <HelpSidebarButton key="help-view-button"/>
        );

        flex.ViewCollection.Content.add(
            <View name="custom-view" key="custom-view">
                <CustomView/>
            </View>
        );

        flex.ViewCollection.Content.add(
            <View name="offersheet-view" key="offersheet-view">
                <OffersheetView/>
            </View>
        );

        flex.ViewCollection.Content.add(
            <View name="offersheets-view" key="offersheets-view">
                <OffersheetsView/>
            </View>
        );

        flex.ViewCollection.Content.add(
            <View name="task-view" key="task-view">
                <TaskView/>
            </View>
        );

        flex.ViewCollection.Content.add(
            <View name="email-view" key="email-view">
                <EmailView/>
            </View>
        );

        flex.ViewCollection.Content.add(
            <View name="calendar-view" key="calendar-view">
                <CalendarView/>
            </View>
        );

        flex.ViewCollection.Content.add(
            <View name="help-view" key="help-view">
                <HelpView/>
            </View>
        );
    }
}
