import { FlexPlugin } from 'flex-plugin';
import React, {useRef} from 'react';
import CustomTaskListComponent from './CustomTaskListComponent';
import TodoListTaskInfoComponent from './TodoListTaskInfoComponent';
import MyAwesomeButton from './MyAwesomeButton';
import PhoneNameComponent from './PhoneNameComponent';
import CustomTaskInfoPanelComponent from './CustomTaskInfoPanelComponent';
import CustomInquiryInfoPanelComponent from './CustomInquiryInfoPanelComponent';
import SmsNameComponent from './SmsNameComponent';
import ChatNameComponent from './ChatNameComponent';
import DefaultNameComponent from './DefaultNameComponent';
import IconColorComponent from './IconColorComponent';
import MuteButtonComponent from './MuteButtonComponent';
import ActiveReservationsComponent from './ActiveReservationsComponent';

import Axios from 'axios';


const PLUGIN_NAME = 'SamplePlugin';

// Some config consts
const config = {
    componentProps: {
        pluginService: {
            enabled: true
        },
        AgentDesktopView: {
            showPanel2: true
        },
        MainHeader: {
            logoUrl: 'https://cdn.pfsuites.com/images/pfs_logo_sm.png'
        },
        LoginView: {
            logoUrl: 'https://cdn.pfsuites.com/images/pfs_logo_sm.png'
        },
        TaskListItem: {
            itemSize: "Small"
        }
    }
};


export default class SamplePlugin extends FlexPlugin {
    constructor() {
        super(PLUGIN_NAME);

    }

    /**
     * This code is run when your plugin is being started
     * Use this to modify any UI components or attach to the actions framework
     *
     * @param flex { typeof import('@twilio/flex-ui') }
     * @param manager { import('@twilio/flex-ui').Manager }
     */
    init(flex, manager) {


        const myOwnChatChannel = flex.DefaultTaskChannels.createChatTaskChannel("custom-manager",
            (task) => task.taskChannelUniqueName === "generic");
                // can modify myOwnChatChannel here

        const inquiryChannel = flex.DefaultTaskChannels.createChatTaskChannel("inquiry",
            (task) => task.taskChannelUniqueName === "generic"

        );
                // can modify myOwnChatChannel here


        manager.strings.TaskHeaderLine = "{{#if task.attributes.channelType}}{{task.attributes.channelType}}{{else}}task{{/if}}: {{#if task.attributes.defaultFrom}}{{task.attributes.defaultFrom}}{{else}}{{task.defaultFrom}}{{/if}}{{#if task.addOns.caller_name.caller_name}}{{task.addOns.caller_name.caller_name}}{{/if}}";



       // manager.strings.TaskExtraInfo = "My task {{task.attributes.name}} was created on {{task.attributes.defaultFrom}}";
        flex.TaskChannels.register(myOwnChatChannel);


        //console.log(props);
        // Updarte Config
        manager.updateConfig(config);

        // Example Button
        flex.TaskCanvasHeader.Content.add(<MyAwesomeButton key="awesome-button" />, {
            sortOrder: -1,
            align: "end",
            if: props => props.task.source.taskChannelUniqueName === "voice"
        });

        // Example Task List Component
        flex.TaskList.Content.add(<CustomTaskListComponent key="demo-component" />, {
            sortOrder: -1,
        });

        // Ater Accept Task
        flex.Actions.addListener("afterAcceptTask", (payload) => {

        });

        // After Send Message
        flex.Actions.addListener("afterSendMessage", (payload) => {

        });

        // After Select Task
        flex.Actions.addListener("afterSelectTask", (payload, abortFunction) => {
            manager.chatClient.channels.on("messageAdded", msg => {
                let channelSid = msg.channel.sid;
                // fetch current task’s channel sid if any
                // compare if it matches message’s channelSid
                let audio = new Audio('https://sunset-bullfrog-1610.twil.io/assets/message_1.mp3');
                audio.volume = 0.5;
                audio.play().catch();
            });
        });

        flex.TaskInfoPanel.Content.replace(<CustomTaskInfoPanelComponent key="custom-info" {...this.props}/>, {
                        sortOrder: -1,
                        if: props => props.task.source.taskChannelUniqueName === "custom-manager"
        });

        flex.TaskInfoPanel.Content.replace(<CustomInquiryInfoPanelComponent key="custom-info" {...this.props}/>, {
                        sortOrder: -1,
                        if: props => props.task.source.taskChannelUniqueName === "inquiry"
        });

        flex.Actions.registerAction("postRequest", (payload) => {
            return Axios.post("http://localhost:8888/ci-flex/index.php/api/contacts",{
                payloadData: payload
            })
                .then(response => {
                    // Update Channel UI with Name

                    /*flex.TaskListButtons.Content.add(<PhoneNameComponent key="phone-name" firstname={response.data.first_name} lastname={response.data.last_name}/>, {
                        sortOrder: -1,
                        if: props => props.task.source.taskChannelUniqueName === "voice"
                    });*/

                    /*flex.TaskListButtons.Content.add(<SmsNameComponent key="sms-name" firstname={response.data.first_name} lastname={response.data.last_name}/>, {
                        sortOrder: -1,
                        if: props => props.task.source.taskChannelUniqueName === "sms"
                    });*/

                    /*flex.TaskListButtons.Content.add(<ChatNameComponent key="chat-name" firstname={response.data.first_name} lastname={response.data.last_name}/>, {
                        sortOrder: -1,
                        if: props => props.task.source.taskChannelUniqueName === "chat"
                    });*/

                    /*flex.TaskListButtons.Content.add(<DefaultNameComponent key="default-name" assignedBy={response.data.email} />, {
                        sortOrder: -1,
                        if: props => props.task.source.taskChannelUniqueName === "custom-manager"
                    });*/

                })
                .catch(error => {
                    console.log(error);
                    throw error;
                });
        });

        flex.Actions.registerAction("taskRequest", (payload) => {
            console.log(payload);
            /*return flex.TaskListItem.Content.add(<DefaultNameComponent key="default-name" assignedBy={payload.task.attributes.assigned_by} requestedAgent={payload.task.attributes.requested_agent}  />, {
                sortOrder: -1,
                if: payload => payload.task.taskChannelUniqueName === "custom-manager"
            });*/
        });

        flex.Actions.addListener("beforeAcceptTask", (payload) => {return flex.Actions.invokeAction("taskRequest",payload)});
        flex.Actions.addListener("beforeAcceptTask", (payload) => {return flex.Actions.invokeAction("postRequest",{sid:payload.sid,phone:payload.task.defaultFrom})});

        // Notifications and alerts
        manager.workerClient.on("reservationCreated", reservation => {

            console.log('dataaaaaa', reservation)

            // console.log(reservation.task);
            // Let's check if the browser supports notifications
            if (!("Notification" in window)) {
                //alert("This browser does not support desktop notification");
            }

            // Let's check whether notification permissions have already been granted
            else if (Notification.permission === "granted") {
                // If it's okay let's create a notification
                if(reservation.task.attributes.call_sid){

                    let audio = new Audio('https://sunset-bullfrog-1610.twil.io/assets/ring_1.mp3');
                    audio.volume = 0.5;  
                    let mute = localStorage.getItem('muteAudio');
                    if(mute=='No'){
                        audio.play().catch();
                    }else{
                        audio.pause();
                    }

                } else {
                    let audio = new Audio('https://sunset-bullfrog-1610.twil.io/assets/message_2.mp3');
                    audio.volume = 0.5;

                    let mute = localStorage.getItem('muteAudio');
                    if(mute=='No'){
                        audio.play().catch();
                    }else{
                        audio.pause();
                    }

                }

                new Notification("Incoming "+ reservation.task.attributes.channelType, {
                        "body": "From:" + reservation.task.attributes.name,
                        "icon": "https://cdn.pfsuites.com/images/pfs_logo_sm.png",
                        "image": "https://cdn.pfsuites.com/images/pfs_logo_sm.png",

                    }
                );
            }

            // Otherwise, we need to ask the user for permission
            else if (Notification.permission !== "denied") {
                Notification.requestPermission().then(function (permission) {
                    // If the user accepts, let's create a notification
                    if (permission === "granted") {
                        new Notification("Incoming "+ reservation.task.attributes.channelType, {
                                "body": "From:" + reservation.task.attributes.name,
                                "icon": "https://cdn.pfsuites.com/images/pfs_logo_sm.png",
                                "image": "https://cdn.pfsuites.com/images/pfs_logo_sm.png",
                            }
                        );
                    }
                });
            }

            // At last, if the user has denied notifications, and you
            // want to be respectful there is no need to bother them any more.

        });

        // After Complete Task
        flex.Actions.addListener("afterCompleteTask", (payload, abortFunction) => {
            Axios.get("http://localhost:8888/ci-flex/index.php/manager/callcenter/archive/"+payload.sid+"/"+payload.task.taskChannelSid)
                .then(response => {
                   // alert("Triggered MyAction with response " + JSON.stringify(response));
                })
                .catch(error => {
                    console.log(error);
                    throw error;
                });
        });


        // CRM callbacks
        flex.CRMContainer.defaultProps.uriCallback = (task) => {
            if(task && task.attributes && task.attributes.channelType === "facebook") {
                //	console.log('is facebook');
            }

            if(task && task.attributes && task.attributes.channelType === "sms") {
                //     console.log('is sms');
            }

            if(task && task.attributes && task.attributes.channelType === "web") {
                //	console.log('is web');
            }

            if(task && task.attributes && task.attributes.name === "Anonymous") {
                //	console.log('is Anon');
            }

            return task ? "http://localhost:8888/ci-flex/index.php/manager/callcenter/crm/" + encodeURI(task.attributes.name) : "http://localhost:8888/ci-flex/index.php/admin" ;
        }


        // Example Task List Component
        flex.TaskListButtons.Content.add(<IconColorComponent key="demo-component" {...this.props} />, {
            sortOrder: -1,
            if: props => props.task.source.taskChannelUniqueName === "custom-manager"
        });

        // Example Task List Component
        flex.TaskListButtons.Content.add(<IconColorComponent key="demo-component" {...this.props} />, {
            sortOrder: -1,
            if: props => props.task.source.taskChannelUniqueName === "inquiry"
        });

         //console.log(audio+'===Sukant')
        // Example Task List Component
        flex.MainHeader.Content.add(<MuteButtonComponent  key="mute-btn" {...this.props}/>, {
            align: 'end',
            sortOrder: -1,
        });

        let workerID = manager.workerClient.sid; 
        // Example Open active reservation List
        flex.TaskList.Content.add(<ActiveReservationsComponent key="active-reservations" workerID= {workerID} {...this.props} />, {
            align: 'start',
            sortOrder: 1,
        });


    }
}