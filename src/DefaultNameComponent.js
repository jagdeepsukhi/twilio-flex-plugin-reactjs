import React from 'react';

const DefaultNameComponent = (props) => {
    console.log(props);

    return <div className="DefaultName">Assigned By: {props.assignedBy} <br /> Assigned To:  {props.requestedAgent}</div>;
};

export default DefaultNameComponent;