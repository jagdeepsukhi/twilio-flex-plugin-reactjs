import React from 'react';

const TodoListTaskInfoComponent = (data) => {
    console.log(data);
    return <div>

        <h1>To Do</h1>
        <ul>
            <li>Answer the task</li>
            <li>Verify Customer Record</li>
            <li>Find the relevant account details</li>
            <li>Resolve the customer's support issue</li>
            <li>Complete the task</li>

            {data.stuff.map(tasks =>
                <li key={tasks.task}>{tasks.task}</li>
            )}
        </ul>
        <hr />
    </div>;
};

export default TodoListTaskInfoComponent;