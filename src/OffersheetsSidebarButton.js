import React from 'react';
import {SideLink, Actions} from "@twilio/flex-ui";

const OffersheetsSidebarButton = ({activeView}) => {
    function navigate() {
        Actions.invokeAction('NavigateToView', {viewName: 'offersheets-view'});
    }
    return(
        <SideLink
            showLabel={true}
            icon={
                <svg width="24" height="24"viewBox="0 0 24 24" fill={"white"}>
                    <path d="M19.5 22.5l4.5-12h-19.5l-4.5 12zM3 9l-3 13.5v-19.5h6.75l3 3h9.75v3z"></path>
                </svg>
            }
            iconActive={
                <svg width="24" height="24"viewBox="0 0 24 24" fill={"white"}>
                    <path d="M19.5 22.5l4.5-12h-19.5l-4.5 12zM3 9l-3 13.5v-19.5h6.75l3 3h9.75v3z"></path>
                </svg>
            }
            isActive={activeView === 'help-view'}
            onClick={navigate}
        >
            Offersheets
        </SideLink>

    );
}

export default OffersheetsSidebarButton