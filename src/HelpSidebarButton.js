import React from 'react';
import {SideLink, Actions} from "@twilio/flex-ui";

const HelpSidebarButton = ({activeView}) => {
    function navigate() {
        Actions.invokeAction('NavigateToView', {viewName: 'help-view'});
    }
    return(
        <SideLink
            showLabel={true}
            icon={
                <svg width="24" height="24"viewBox="0 0 24 24" fill={"white"}>
                    <path d="M10.5 16.5h3v3h-3zM16.5 6c0.828 0 1.5 0.672 1.5 1.5v4.5l-4.5 3h-3v-1.5l4.5-3v-1.5h-7.5v-3h9zM12 2.25c-2.604 0-5.053 1.014-6.894 2.856s-2.856 4.29-2.856 6.894c0 2.604 1.014 5.053 2.856 6.894s4.29 2.856 6.894 2.856c2.604 0 5.053-1.014 6.894-2.856s2.856-4.29 2.856-6.894c0-2.604-1.014-5.053-2.856-6.894s-4.29-2.856-6.894-2.856zM12 0v0c6.627 0 12 5.373 12 12s-5.373 12-12 12c-6.627 0-12-5.373-12-12s5.373-12 12-12z"></path>
                </svg>
            }
            iconActive={
                <svg width="24" height="24"viewBox="0 0 24 24" fill={"white"}>
                    <path d="M10.5 16.5h3v3h-3zM16.5 6c0.828 0 1.5 0.672 1.5 1.5v4.5l-4.5 3h-3v-1.5l4.5-3v-1.5h-7.5v-3h9zM12 2.25c-2.604 0-5.053 1.014-6.894 2.856s-2.856 4.29-2.856 6.894c0 2.604 1.014 5.053 2.856 6.894s4.29 2.856 6.894 2.856c2.604 0 5.053-1.014 6.894-2.856s2.856-4.29 2.856-6.894c0-2.604-1.014-5.053-2.856-6.894s-4.29-2.856-6.894-2.856zM12 0v0c6.627 0 12 5.373 12 12s-5.373 12-12 12c-6.627 0-12-5.373-12-12s5.373-12 12-12z"></path>
                </svg>
            }
            isActive={activeView === 'help-view'}
            onClick={navigate}
        >
            Help
        </SideLink>

    );
}

export default HelpSidebarButton