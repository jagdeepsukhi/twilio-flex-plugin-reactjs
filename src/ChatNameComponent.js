import React from 'react';

const ChatNameComponent = (props) => {
    console.log(props);
    return <div className="ChatName">{props.firstname + " " + props.lastname}</div>;
};

export default ChatNameComponent;