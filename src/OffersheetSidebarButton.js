import React from 'react';
import {SideLink, Actions} from "@twilio/flex-ui";

const OffersheetSidebarButton = ({activeView}) => {
    function navigate() {
        Actions.invokeAction('NavigateToView', {viewName: 'offersheet-view'});
    }
    return(
        <SideLink
            showLabel={true}
            icon={
                <svg width="24" height="24"viewBox="0 0 24 24" fill={"white"}>
                    <path d="M9 0l-9 12h9l-6 12 21-15h-12l9-9z"></path>
                </svg>
            }
            iconActive={
                <svg width="24" height="24"viewBox="0 0 24 24" fill={"white"}>
                    <path d="M9 0l-9 12h9l-6 12 21-15h-12l9-9z"></path>
                </svg>
            }
            isActive={activeView === 'help-view'}
            onClick={navigate}
        >
            Offersheet
        </SideLink>

    );
}

export default OffersheetSidebarButton