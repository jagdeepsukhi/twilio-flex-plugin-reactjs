import React from 'react';
import {SideLink, Actions} from "@twilio/flex-ui";


const buttonStyles = {
    alignSelf: 'center',
    height: '28px',
    fontSize: '10px',
    fontWeight: 'bold',
    letterSpacing: '2px',
    whiteSpace: 'nowrap',
    color: 'rgb(255, 255, 255)',
    padding: '0px 16px',
    borderWidth: 'initial',
    borderStyle: 'none',
    borderColor: 'initial',
    borderImage: 'initial',
    marginLeft: '15px',
    textTransform: 'uppercase',
    background: 'rgb(209, 25, 123)',
    outline: 'none',
    borderRadius: '100px',
};


const MyAwesomeButton = ({activeView}) => {
    function navigate() {
        Actions.invokeAction('NavigateToView', {viewName: 'custom-view'});
    }
    return(
        <button
            style={buttonStyles}
            onClick={navigate}
        >Do Something</button>
    );
}

export default MyAwesomeButton