import * as FlexPlugin from 'flex-plugin';
import SamplePlugin from './SamplePlugin';
import AddPluginView from './AddCustomViewPlugin';
import ActionPlugin from './ActionPlugin';
import ChatVisualNotificationsPlugin from './ChatVisualNotificationsPlugin';
import RecordControlsPlugin from './RecordControlsPlugin';

FlexPlugin.loadPlugin(RecordControlsPlugin);
FlexPlugin.loadPlugin(ChatVisualNotificationsPlugin);
FlexPlugin.loadPlugin(SamplePlugin);
FlexPlugin.loadPlugin(AddPluginView);

