import React from 'react';
import {SideLink, Actions} from "@twilio/flex-ui";

const CalendarSidebarButton = ({activeView}) => {
    function navigate() {
        Actions.invokeAction('NavigateToView', {viewName: 'calendar-view'});
    }
    return(
        <SideLink
            showLabel={true}
            icon={
                <svg width="24" height="24" viewBox="0 0 24 24" fill={"white"}>
                    <path d="M7.5 9h3v3h-3zM12 9h3v3h-3zM16.5 9h3v3h-3zM3 18h3v3h-3zM7.5 18h3v3h-3zM12 18h3v3h-3zM7.5 13.5h3v3h-3zM12 13.5h3v3h-3zM16.5 13.5h3v3h-3zM3 13.5h3v3h-3zM19.5 0v1.5h-3v-1.5h-10.5v1.5h-3v-1.5h-3v24h22.5v-24h-3zM21 22.5h-19.5v-16.5h19.5v16.5z"></path>
                </svg>
            }
            iconActive={
                <svg width="24" height="24" viewBox="0 0 24 24" fill={"white"}>
                    <path d="M7.5 9h3v3h-3zM12 9h3v3h-3zM16.5 9h3v3h-3zM3 18h3v3h-3zM7.5 18h3v3h-3zM12 18h3v3h-3zM7.5 13.5h3v3h-3zM12 13.5h3v3h-3zM16.5 13.5h3v3h-3zM3 13.5h3v3h-3zM19.5 0v1.5h-3v-1.5h-10.5v1.5h-3v-1.5h-3v24h22.5v-24h-3zM21 22.5h-19.5v-16.5h19.5v16.5z"></path>
                </svg>
            }
            isActive={activeView === 'calendar-view'}
            onClick={navigate}
        >
            Calendar
        </SideLink>

    );
}

export default CalendarSidebarButton