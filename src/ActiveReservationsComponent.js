import React from 'react';

const trashStyles = {
    alignSelf: 'center',
    height: '28px',
    width: '28px',
    color: 'red',
    fontSize: '22px'
};
const buttonStyles = {
     
};


const iconStyles = {
    display: 'flex',
    color: '#000',
    flex: '0 0 44px',
}

const iconLabelStyles = {
    background: '#FF0000',
}

const liStyle = {
       borderBottom: '1px solid rgb(204, 204, 204)'
}


const filterButton = {
    border: 'none',
    background: 'transparent',
    fontWeight: 'bold',
    marginLeft: '-8px',
    color: 'rgb(34, 34, 34)',
    margin: '6px 0'
}


class ActiveReservationsComponent extends React.Component {


    constructor(props){
        super();
        
        //console.log("ripan",props.workerID); 

        this.state = {
              error: null,
              isLoaded: false,
              items: [],
              workerID: props.workerID,
              filter: 1
        };
        this.changeFilter = this.changeFilter.bind(this);
    }

    componentDidMount() {
           // Call this function so that it fetch first time right after mounting the component
          this.getReservations(this.state.filter);
          setInterval(() =>this.getReservations(this.state.filter), 10000);

    }
 
    getReservations(selectedFilter){
      try {
            (async () => {
                var workerID = '';

                if(selectedFilter==2){
                   workerID = this.state.workerID;
                } 
                const res = await fetch('http://localhost/ci-flex/index.php/api/contacts/reservations?workerID='+workerID);
                const result = await res.json();
                this.setState({
                    isLoaded: true,
                    items: result
                  });
            })();
          } catch(e) {
            console.log(e);
            this.setState({
                isLoaded: true,
                error:e
            });
          }
    }

   changeFilter(event) {
        var selectedFilter = event.target.value;         
        this.getReservations(selectedFilter);
        this.setState({ filter:selectedFilter });
        
    } 

    render() {
        const { error, isLoaded, items } = this.state;
         if (error) {
            return <div>Error: {error.message}</div>;
        } else if (!isLoaded) {
            return <div>Loading...</div>;
        }else {





        return <div style={buttonStyles} >

            <div className="Twilio-ModalPopupWithEntryControl css-g2rjcl">
            <button className="Twilio-TaskListFilter-TaskFilterButton css-19j9h9r">
                <select className="Twilio" style={filterButton} onChange={this.changeFilter}  value={this.state.filter}>
                  <option value="1" >All Active Reservations</option>
                  <option value="2" >Assigned</option>
                </select>
                 
            </button>
            </div>
            <ul>
            {items.map(item => (
            
            

             <li key={item.task_id} style = {liStyle} className="Twilio-TaskListBaseItem css-78t99f">   

                <div className="Twilio-TaskListBaseItem-UpperArea" style={iconStyles}>
                    <div className="Twilio-Icon Twilio-Icon-GenericTaskBold Twilio-TaskListBaseItem-IconArea css-1m037yc" style={iconLabelStyles}>
                        <svg width="1em" height="1em" viewBox="0 0 24 24" className="Twilio-Icon-Content"><g fill="none" fill-rule="evenodd"><path d="M0 0h24v24H0z"></path><path d="M16.544 5.607c.384 0 .722.145 1.016.435.293.29.44.625.44 1.005V17.56c0 .38-.147.715-.44 1.005-.294.29-.632.435-1.016.435H6.456c-.384 0-.722-.145-1.016-.435-.293-.29-.44-.625-.44-1.005V7.047c0-.38.147-.714.44-1.005.294-.29.632-.435 1.016-.435H9.4c.113-.469.367-.854.762-1.155.395-.301.84-.452 1.337-.452s.942.15 1.337.452c.395.301.65.686.762 1.155h2.945zm-5.044.047a.683.683 0 0 0-.508.207.708.708 0 0 0-.203.517c0 .207.068.385.203.534.136.15.305.224.508.224a.657.657 0 0 0 .508-.224.768.768 0 0 0 .203-.534.708.708 0 0 0-.203-.517.683.683 0 0 0-.508-.207zm-1.428 9.38l-1.542-1.57a.433.433 0 0 0-.621.003.457.457 0 0 0 .003.637l1.447 1.473a1 1 0 0 0 1.427 0l4.3-4.378a.457.457 0 0 0 0-.64.44.44 0 0 0-.629 0l-4.385 4.475z" fill="currentcolor" fill-rule="nonzero"></path></g></svg>
                    </div>
                    <div className="Twilio-TaskListBaseItem-Content css-1ou7mv4">
                        <div className="Twilio-TaskListBaseItem-FirstLine css-zyj899">
                            <span className="Twilio">{item.task_id.substring(0,15)+"..."}</span>
                        </div>
                        <div className="Twilio-TaskListBaseItem-SecondLine css-qi0a2c">
                            <span className="Twilio">Assigned</span>
                        </div>


                    </div>


                    <div class="Twilio-TaskListBaseItem-Actions css-q7usly">
                    <div style={ trashStyles }>
                       <svg version="1.1" xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 32 32">
                        <title>bin</title>
                        <path d="M4 10v20c0 1.1 0.9 2 2 2h18c1.1 0 2-0.9 2-2v-20h-22zM10 28h-2v-14h2v14zM14 28h-2v-14h2v14zM18 28h-2v-14h2v14zM22 28h-2v-14h2v14z"></path>
                        <path d="M26.5 4h-6.5v-2.5c0-0.825-0.675-1.5-1.5-1.5h-7c-0.825 0-1.5 0.675-1.5 1.5v2.5h-6.5c-0.825 0-1.5 0.675-1.5 1.5v2.5h26v-2.5c0-0.825-0.675-1.5-1.5-1.5zM18 4h-6v-1.975h6v1.975z"></path>
                        </svg>

                    </div>
                    </div>



                     
                </div>
            </li>


            ))}


             </ul>



           






        </div>;
        }
    }


}

export default ActiveReservationsComponent;